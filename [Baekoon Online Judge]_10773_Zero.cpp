//https://www.acmicpc.net/problem/10773

#include <iostream>
#include <algorithm>
#include <stack>
#include <string>
#include <vector>

using namespace std;
int K, a, sum;

int main()
{  
 vector <int> v;
 cin >> K;
 for(int i = 0; i < K; i++)
 {
   cin >> a;
   if(a != 0) v.push_back(a);
   else v.pop_back();
 }
 for(int j = 0; j < v.size(); j++)
 {
   sum += v[j];
 }
 cout << sum;
}